plugin.tx_contentcat {
	view {
		templateRootPath = {$plugin.tx_contentcat.view.templateRootPath}
		partialRootPath = {$plugin.tx_contentcat.view.partialRootPath}
		layoutRootPath = {$plugin.tx_contentcat.view.layoutRootPath}
	}
	features {
		rewrittenPropertyMapper = 1
		skipDefaultArguments = 1
	}
	settings {
		categoryFooterKey = {$plugin.tx_contentcat.settings.categoryFooterKey}
		categoryListPageUid = {$plugin.tx_contentcat.settings.categoryListPageUid}
	}
}

lib.tx_contentcat.categoryFooter = USER
lib.tx_contentcat.categoryFooter {
	userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
	vendorName = t3easy
	extensionName = Contentcat
	pluginName = Cat
	controller = Content
	action = footer
	switchableControllerActions.Content.1 = footer
}

tt_content.text.{$plugin.tx_contentcat.settings.categoryFooterKey} < lib.tx_contentcat.categoryFooter
tt_content.image.{$plugin.tx_contentcat.settings.categoryFooterKey} < lib.tx_contentcat.categoryFooter
tt_content.textpic.{$plugin.tx_contentcat.settings.categoryFooterKey} < lib.tx_contentcat.categoryFooter
tt_content.table.{$plugin.tx_contentcat.settings.categoryFooterKey} < lib.tx_contentcat.categoryFooter
tt_content.media.{$plugin.tx_contentcat.settings.categoryFooterKey} < lib.tx_contentcat.categoryFooter
tt_content.multimedia.{$plugin.tx_contentcat.settings.categoryFooterKey} < lib.tx_contentcat.categoryFooter

plugin.tx_contentcat._CSS_DEFAULT_STYLE (
	.tx-contentcat .info {
		color: red;
	}
	.tx-contentcat ul.content-list {
		margin: 0;
		padding: 0;
		list-style-type: none;
	}
	.tx-contentcat ul.content-list li {
		font-size: 1em;
		border-bottom: 0.063em solid #DDDDDD;
	}
	.tx-contentcat ul.content-list li.last {
		border-bottom: none;
	}
	.tx-contentcat-footer {
		margin-top: 1em;
	}
	.tx-contentcat-footer ul {
		margin: 0;
		padding: 0;
		list-style-type: none;
	}
	.tx-contentcat-footer ul li {
		display: inline;
	}
)
