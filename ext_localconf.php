<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	't3easy.' . $_EXTKEY,
	'Cat',
	array(
		'Category' => 'list',
		'Content' => 'list, footer'
	),
	// non-cacheable actions
	array(
		'Category' => '',
		'Content' => '',
	)
);
?>