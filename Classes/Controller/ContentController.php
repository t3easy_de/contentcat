<?php
namespace t3easy\Contentcat\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012 - 2013 Jan Kiesewetter <janYYYY@t3easy.de>, t3easy
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package contentcat
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class ContentController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * contentRepository
	 *
	 * @var \t3easy\Contentcat\Domain\Repository\ContentRepository
	 * @inject
	 */
	protected $contentRepository;

	/**
	 * action list
	 * @param \TYPO3\CMS\Extbase\Domain\Model\Category $category
	 * @return void
	 */
	public function listAction(\TYPO3\CMS\Extbase\Domain\Model\Category $category) {
		\Tx_Flextend_Service_ExtendTitle::extendTitle($category->getTitle());
		$contents = $this->contentRepository->findByCategory($category->getUid());
		$this->view->assign('contents', $contents);
		$this->view->assign('category', $category);
	}

	/**
	 * Render the footer
	 * @return void
	 */
	public function footerAction() {
		$contentElement = $this->contentRepository->findByUid(
			$this->configurationManager->getContentObject()->data['uid']
		);
		$this->view->assign('contentElement', $contentElement);
	}

}
?>